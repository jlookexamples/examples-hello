package com.example.myproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class HelloApplication {
	
	@RequestMapping("/")
	public String sayHello() {
		return "Welcome spring boot application";
	}
	
	public static void main(String[] args) {
		SpringApplication.run(HelloApplication.class, args);
	}

}
